paperbuilder
============

Build scientific papers

Usage
-----

Installation
------------

Requirements
^^^^^^^^^^^^

Compatibility
-------------

Licence
-------

Authors
-------

`paperbuilder` was written by `Fabian Kloosterman <fabian.kloosterman@nerf.be>`_.
