.. _`command_line`:

Command line interface
======================

.. autoprogram:: paperbuilder.cli:parser
   :prog: paper

.. Paper project commands
.. ----------------------

.. .. autoprogram:: paperbuilder.cli:parser
..   :start_command: init
..   :prog: paper

.. Paper version commands
.. ----------------------

.. .. autoprogram:: paperbuilder.cli:parser
..    :start_command: create
..    :prog: paper

.. .. autoprogram:: paperbuilder.cli:parser
..    :start_command: remove
..    :prog: paper

.. .. autoprogram:: paperbuilder.cli:parser
..    :start_command: build
..    :prog: paper

.. .. autoprogram:: paperbuilder.cli:parser
..    :start_command: clean
..    :prog: paper

.. Analysis commands
.. -----------------

.. .. autoprogram:: paperbuilder.cli:parser
..    :start_command: analysis create
..    :prog: paper

.. .. autoprogram:: paperbuilder.cli:parser
..    :start_command: analysis remove
..    :prog: paper

.. .. autoprogram:: paperbuilder.cli:parser
..    :start_command: analysis build
..    :prog: paper

.. .. autoprogram:: paperbuilder.cli:parser
..    :start_command: analysis clean
..    :prog: paper