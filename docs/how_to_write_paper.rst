Writing and building a paper
============================

The text of a paper is written in pandoc flavored Markdown, which is a markup language 
with plain text formatting syntax. The text source file is located in the ``text`` folder
of a paper version and the name of file should be ``<basename>.md``, where ``<basename>``
is set by the ``text_basename`` configuration option and defaults to ``manuscript``. 

The text may also be split across multiple source files. In this case, the file names
should match the pattern ``<basename>-<seq>-<description>.md``, where ``<seq>`` is a
sequence number and the description is optional. For example: ``manuscript-00.md`` or 
``manuscript-01-introduction.md``. Source files are concatenated in order of their
sequence number. Note: make sure that all source files end with an empty line for
concatenation to work as expected.

The text source file may (and often does) start with a YAML metadata block (see :ref:`metadata`)
that starts and ends with ``---`` on a single line. When multiple source files are used, 
generally only the first one contains a YAML metadata block. Alternatively, the metadata block
can be put in a separate file ``<basename>.yaml``.

A brief overview of the most important Markdown syntax features is given in the sections below.
To get a more in-depth and complete picture, read up on
`Markdown basic syntax <https://www.markdownguide.org/basic-syntax/>`_ and the
`pandoc default Markdown extensions <https://pandoc.org/MANUAL.html#pandocs-markdown>`_.

Several pandoc filters are automatically enabled and are also explained below, including 
`pandoc-citeproc <https://pandoc.org/MANUAL.html#citations>`_ for citations,
`pandoc-eqnos <https://github.com/tomduck/pandoc-eqnos>`_ for numbering equations,
`pandoc-tablenos <https://github.com/tomduck/pandoc-tablenos>`_ for numbering tables,
`pandoc-figurenos <https://github.com/tomduck/pandoc-fignos>`_ for numbering figures,
`pantable <https://github.com/ickc/pantable>`_ for importing table data from csv files
and (when available) `pandoc-scholar <https://github.com/pandoc-scholar/pandoc-scholar>`_
for handling author and affiliation metadata.

Writing Markdown
----------------

Basics
......

The following example shows off basic pandoc flavored Markdown syntax.

.. code-block:: md

  # Heading level 1

  First paragraph. Paragraphs are separated by an empty line.

  Thus this is the second paragraph.

  ## Heading level 2

  Text can be emphasized: _emphasis_ or **strong emphasis**.
  In addition, you can ~~strikeout~~ text and use superscript: 10^2^ and subscript: H~2~O.
    
  Lists are defined as follows:

  1. item 1
     1. nested list
  2. item 2
     with second line
  3. item 3

  And unnumbered lists:

  - item 1
    - nested list
  - item 2
    with second line
  - item 3

  You can include code with syntax highlighting:

  ```python
  import numpy as np
  a = np.array([1,2,3])
  ```

  Or if you would like to number the code lines:

  ``` {.python .numberLines startFrom="1"}
  import numpy as np
  a = np.array([1,2,3])
  ```

  Equations, such as $E=mc^2$, are supported too and follow TeX math syntax.


.. _`metadata`:

Metadata: authors and  affiliations
...................................

The pandoc-scholar extension will parse title, author and institute fields in the 
YAML metadata block and inserts this information into the main text with proper formatting.
The ``author`` field contains a list of authors and for each author you indicate the institute(s)
they below to and their email address. You can also indicate if authors are equal contributors or
corresponding authors. The ``institute`` field contains a list of institutes and their addresses.
Here is an example:

.. code-block:: yaml
  
  ---
  title: "This is the paper title"
  author:
    - John Doe:
      institute:
        - cfk
      email: john.doe@example.com
      equal_contributor: True
    - Jane Doe:
      institute:
        - cfk
        - ioi
      email: jane.doe@example.com
      equal_contributor: True
    - Jim Jones:
      institute:
        - ioi
      email: jim.jones@example.com
    - Jeff Jake Johnson:
      institute:
        - cfk
      email: jeff.johnson@example.com
      address: Main Street 10, Big City, FarAway
      correspondence: "yes"
  institute:
    - cfk:
      name: Center for all that is Known
      address: Main Street 10, Big City, FarAwayCountry
    - ioi: Institute of the Invisible, Small Town, NextDoor
  ---

Citations
.........

**Bibliography file**

To cite articles and create a list of references in the paper, a bibliography file in 
BibTeX format that contains article records is required. The location of this file needs
to be configured through the ``bibliography_path`` and ``bibliography_file`` options 
(see :ref:`configuration`).

Although it is possible to generate the BibTeX file by hand by copy/paste of BibTeX records
in an editor, it is easier to have the file auto-generated. If you use Mendeley, you can enable
BibTeX synchronization in the Tools/Options menu (BibTeX tab). You could either create one BibTeX
file for the whole library or one file per group. In the latter case, you could for example set up
a separate group for each paper project.

**Citation style**

The style of citations is governed by a citation style file. The location of this file needs 
be configured through the ``cls_path`` and ``csl_file`` options (see :ref:`configuration`).
Citation style files can be downloaded from https://github.com/citation-style-language/styles.

**Insert citations**

To insert citations in the text: ``Some people have shown something [@Johnson2015; @Jones2017].``

To omit the authors in a citations: ``Hansen et al. did not show the same [-@Hansen2016].``

In text citations: ``@anderson2014 said a lot too.``

**Insert bibliography**

By default, the bibliography will be inserted at the end of the document. To control the
insertion of the reference list yourself, insert the following where you want the bibliography
to appear:

::

  <div id="refs"></div>


.. _`insert_variables`:

Insert variable values
......................

Summary data saved as YAML files in the ``variables`` folder can be referred to in the text
document. During the a preprocessing step, those references will be resolved and substituted by
the data value from the YAML files (using the tool Jinja2). Since collection of YAML files in the ``variables`` folder are
consolidated into a single nested dictionary, you will have to make sure that each variable has a
unique name.

To refer to a variable in the text you use dot-notation in between start marker ``<<*`` and
end marker ``*>>``. For the following YAML file:

.. code-block:: yaml
  
  stats:
    mean: 3.4
    std: 0.8
  tests:
    ttest:
      pvalue: 0.02
  message: Hello World!
  

you would for example write ``The mean equals <<*stats.mean*>>.`` to refer to mean variable.

In addition to variable substitution, you can also apply filters to the variables to alter their 
value or formatting. For example, to round a value to the nearest integer you could do:
``<<*stats.mean | round*>>``. Notice how filters are separated by a vertical bar. Multiple filters
can be applied in sequence, e.g. ``<<*message | replace("World", "Universe") | lower*>>``.
Here, the second filter (``lower``) is applied to the output of the first (``replace``). Note that
filters can take extra arguments (like the ``replace`` filter here).

There are a number of `built-in filters <http://jinja.pocoo.org/docs/2.10/templates/#builtin-filters>`_
that come with Jinja2. PaperBuilder also ships with a number of its own built-in filters to format
common statistical tests and p-values:

- **pval(p)**: format p-value
- **test2(test, labels=None, info=True)**: show results of two sample test.

To add custom filters, configure the ``filters`` option (see :ref:`configuration`). A valid filter is
a function that takes at least one argument (unless it is meant to generate a value) and returns one
output value.


Insert figures
..............

To insert numbered figures with caption, you use the following syntax:

.. code-block:: md
   
   ![This is the figure caption. ](figures/figure.png){#fig:behavior}
   
   ![This is yet another figure caption. ](figures/figure_suppl.png){#fig:behavior_suppl tag="S1"}

The text in square brackets is the caption of the figure and the path to the image is placed
within the parentheses. The extra attributes at the end in the curly braces give the figure an
identifier for references in the text and an optional tag to be used instead of the automatic figure
number (i.e "S1" in the second example above).

To refer to these figures in the text, you would use ``figure {@fig:behavior}a``. Note that you still
have to prepend the word `figure` nd append panel labels is needed.


.. _`insert_tables`:

Insert tables
.............

There are four kind of tables that are directly inserted into the text and which are explained in 
the `pandoc table documentation <https://pandoc.org/MANUAL.html#tables>`_. Like figures, tables can
be given an identifier attribute for referencing in the text. For example:

.. code-block:: md
  
  A B
  - -
  0 1
  
  Table: Caption. {#tbl:table1}

To reference the table, one would write ``table {@tbl:table1}``.

It is also possible to insert tables from a csv file (courtesy of the 
`pantable extension <https://github.com/ickc/pantable>`_). Here is an example:

::

  ```table 
  ---
  caption: 'caption {#tbl:table}'
  alignment: LCCCCC
  width: [0.4, 0.1, 0.2, 0.1, 0.1, 0.1]
  markdown: true
  include: tables/table.csv
  ---
  ```

Note that the code block between the triple backticks contains a YAML section with 
table options. The ``include`` option specifies the path to the csv file. If ``markdown``
is true, then both caption and table cells can contain Markdown syntax. The ``alignment``
option specifies the text alignment for each column (L=left, C=center and R=right).
The ``width`` option specifies the relative width of the columns. Note also that the
table identifier ``{#tbl:table}`` is part of the caption.

Insert equations
................

Equations are written between two ``$`` and use TeX math syntax. They can either appear in
line with text or on a separate line. An equation can be marked with an identifier for
referencing in the text. For example:

.. code-block:: md
  
  $x=\cos(\phi)*\sin(\theta)$ {#eq:formula}
  
  See equation {@eq:formula}. And what about $y=\sum_i{x_i}$?


Document styling
----------------

Styling of the output document is done using either a template (in case of PDF output)
or a reference document (in case of DOCX output). Both template file and reference document
can be configured using the ``latex_template`` and ``docx_reference`` options
(see :ref:`configuration`).

For DOCX output, the styles in the reference document (but not its content) is used to produce
the final output. Styling may include font sizes, line spacing, the presence of line numbers,
margin sizes and whether or not sections are numbered. It is recommended that the reference
document is a modified version of the docx file produced by PaperBuilder.

For PDF output, the template provides layout and styling. Some of the options can be set in
the YAML metadata block or otherwise added using Latex commands. For example, to set the margins to 2 cm,
duse double line spacing, have numbered sections and line numbers, you would use the following
lines in your Markdown document (note that the last two lines are outside the YAML metadata block):

.. code-block:: md

  ---
  geometry: margin=2.0cm
  numbersections: True
  header-includes:
    - \usepackage{setspace}
    - \usepackage{lineno}
  ---
  
  \doublespacing
  \linenumbers


.. _`how_to_build_paper`:

Building a paper
----------------

(to be completed)

targets and dependencies

force build

.. code-block:: bash

  paperbuilder build --version draft
