Getting started
===============

Installation
------------

PaperBuilder is easily installed using `conda <https://conda.io/docs/>`_.
First, make sure you have a working Python distribution (e.g. by installing
`miniconda <https://conda.io/docs/user-guide/install/index.html>`_). Next,
install PaperBuilder from a terminal:

``conda install paperbuilder -c KloostermanLab``

This will install PaperBuilder and most of its dependencies. There are a few
dependencies that need to be installed separately:

pandoc-scholar provides pandoc filters that parse and format scholarly metadata,
such as authors and affiliations. Note that if you will not make use of the features
provided by pandoc-scholar, then installation is optional. However, most of the examples
in the documentation assume that pandoc-scholar is installed. To install, download the 
`latest release <https://github.com/pandoc-scholar/pandoc-scholar/releases/latest>`_
and unpack the file somewhere on your system. Next, configure PaperBuilder so that 
it can find the installed pandoc-scholar files:

``paper config -u pandoc_scholar_path /path/to/pandoc-scholar``

`inkscape <https://inkscape.org/>`_ is used for creating figure layouts and for
converting SVG files to PNG images. The program can be downloaded from their website.
Alternatively, install inkscape using your platform's package management tools.

`FigureFirst <https://figurefirst.readthedocs.io>`_ is used for building figures from
layouts defined as a svg file. It comes with extensions for inkscape that make it easier
to tag figures and groups of figures. See the 
`documentation <https://figurefirst.readthedocs.io/en/latest/installation.html>`_ for how
to install the extensions.

To build PDF output, pandoc uses the TeX document production system (in particular
the xelatex engine). To fullfil this requirement, it is easiest to install the
`TeXLive software package <https://www.tug.org/texlive/>`_. For example, in Ubuntu you
could do:

::
  
  apt-get install texlive

You may also need to install extra packages, such as texlive-fonts-extra, 
texlive-math-extra and lmodern (a font).


**List of dependencies**

- python 3.6 or up
- yaml
- pandoc
- pandoc-eqnos
- pandoc-tablenos
- pandoc-fignos
- pantable
- numpy
- matplotlib
- jinja2
- appdirs
- doit
- figurefirst (optional, only if svg layouts are used)
- pandoc-scholar (optional)

**Dependencies for building documentation**

- sphinx
- sphinx-rtd-theme
- sphinxcontrib-autoprogram


Tutorial
--------

Step 1: Set up new paper project
................................

To set up a new paper project, first create an empty folder and then call ``paper init``.
It is good practice to provide a description of the project:

::
  
  mkdir my_paper
  paper init ./my_paper --description "A paper about my great results."

If you pass the ``--create`` option, then the project folder will be generated automatically.
If the project folder is not empty and you still want to initialize it, use the ``--force`` option.
The name of the project is taken from the folder name, but you can set it explicitly
using the ``--name`` option.

By default, a first paper version `draft` is created in the project. To specify another name for
this initial paper version, use the ``--version`` option. 

An initialized paper project also contains a `code` folder with a Python package structure that
you can use for all Python code that is specific for the project. By default, the package structure
is ``papers.<paper-name>``. If you prefer a different package structure, use the ``--package`` option.
To use the Python package, you first need to put it on the Python path, for example using:

::

  conda develop /path/to/paper/project/code


Step 2: Manage paper versions
.............................

To create a new paper version in an exising project, use:

::

  paper create revision1

You can also specify the path to the paper project explicitly (e.g. if the current working
directory is not a paper project), using the ``--path`` option.

If you like to build on a previous paper version, you can use the ``--clone`` option. The argument
to the ``--clone`` option is either the name of a paper version in the current project, or
of the form ``/path/to/project:version`` that specifies a paper version in another project.

To make the new paper version the `current` version, use the ``--current`` flag. The current paper
version is the default target for all commands that require a specific version (unless the command
is issued from a paper version folder).

To remove a paper version, use:
::

  paper remove old_version

Note that this command will remove the paper version without further warning!


Step 3: Build paper
...................

To perform preprocessing and build PDF/DOCX output files, issue the following
command from a paper version folder:

::

  paper build

When this command is called from a paper project folder, either the configured
`current` version is built or you need to explicitly specify the paper version
using the ``--version`` option. To change the current version at the project level,
use (where ``draft`` will be set as the new current paper version):

::

  paper config --project current draft

PaperBuilder keeps track of the build status of the output files and intermediate files,
and knows when dependencies have (not) changed. So, issuing the build command again when
nothing has changed will not result in a rebuild. If you need to force a rebuild regardless
of changes in dependencies, use the ``--force`` flag.

If you would only want to execute preprocessing, then use the ``preprocess`` command
instead of the ``build`` command.

To clean the output of the preprocess and build commands, use:

::

  paper clean


Step 4: Manage analyses (summaries, tables and figures)
.......................................................

To set up a new analysis in the current paper version, use:

::

  paper analysis create analysis_name

Use the ``--version`` option to explicitly set a paper version, and the ``--path`` option
to explicitly set the path to a paper project.

To build on another analysis, you can use the ``--clone`` option. The argument to the 
clone option is either the name of an analysis in the current paper version, or 
of the form ``version:analysis`` to specify an analysis in another version in the same project,
or of the form ``/path/to/paper/version:analysis`` to specify an analysis in another paper project.

A newly created analysis will have example files to get you started.

To remove one or more existing analyses, use:

::

  paper analysis remove analysis1 analysis2

Again, you can use the ``--version`` and ``--path`` options to explicitly set 
paper version and project if needed.
To remove all analyses, do not specify individual analysis names and use the
 ``--all`` flag. To also remove all the outputs of the removed analyses, use
the ``--clean`` flag.


Step 5: Execute analyses
........................

Finally, to execute one or more specific analyses, use:

::

  paper analysis build analysis1 analysis2

Again, you can use the ``--version`` and ``--path`` options to explicitly set 
paper version and project if needed. To build all analyses, do not specify individual
analysis names and use the ``--all`` flag. To force execution of analyses, even if
dependencies haven't changed, use the ``--force`` flag.

Use the ``--target`` option to select specific analysis targets. For example ``--target summary*``
will only build all summaries (similar for ``figure*`` and ``table*``). To build specific
summaries/tables/figures, you can do as follows: ``--target figure-main``
(where main is the figure name). Multiple targets can be specified for a single build command.

To remove the outputs of the analysis build command, use:

::

  paper analysis clean analysis1 analysis2

The clean command also accepts the ``--version``, ``--path``, ``--all`` and ``--target``
options as described above.

