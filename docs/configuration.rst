.. _`configuration`:

Configuration options
=====================

PaperBuilder is configurable at multiple levels:

- **user**: stored in a standard location on the user's system. In Ubuntu, this
  will be ``~/.config/paperbuilder/config.yaml``. Options set at this level will affect all 
  paper projects.
- **project**: ``config.yaml`` stored in project's root folder. Options set at this
  level will override the user configuration, but only affect paper versions in the
  given project.
- **version**: ``config.yaml`` in paper version folder. Options set at this level
  will override user and project configuration, but only affect the given paper version.

Available configuration options
-------------------------------

- **editor**: name or path to the text editor that will be used to open configuration
  files for the ``paperbuilder config --edit`` command. (default None)
- **pandoc_scholar_path**: full path to pandoc-scholar package. (default None)
- **current**: the name of the currently active paper version in a project. This value
  is used when a command requires a paper version, but none is given. By setting this
  option in a project ``config.yaml file``, one can omit the ``--version`` argument.
  (default None)
- **csl_path**: full path to the location of citation style files. Style files can be 
  obtained at https://github.com/citation-style-language/styles. (default None)
- **csl_file**: name of the citation style file. (default None)
- **bibliography_path**: full path to the location of bibtex bibliography files. 
  (default None)
- **bibliography_file**: name of the bibtex bibliography file to look up citations. 
  (default None)
- **latex_template**: full path to the latex template for generating PDF output.
  By default the template that comes with pandoc or pandoc-scholar (if installed) is used.
  (default None)
- **docx_reference**: full path to the docx reference file for styling docx output files.
  (default None)
- **build_target**: list of output targets to build if not specified on the command line.
  (default ['pdf', 'docx'])
- **text_basename**: base name of the Markdown text file. This value will be used when creating 
  a new paper version and also when building the paper. In principle, one could use this
  to have multiple alternative text files in the same paper version and switch between these
  files by changing the ``filename`` configuration option. (default 'manuscript')
- **filters**: list of strings specifying importable dictionaries with filters for
  the preprocessing step. E.g. ``package.module.filters`` will attempt to import the variable
  ``filters`` from the module ``package.module``. Unlike other configuration options, the
  filters defined at user, project and paper version levels are all combined into a single list.


Editing configuration options
-----------------------------

There are two ways to set configuration options: either by directly editing the ``config.yaml``
files or by using the command-line interface. Executing the following on the command line in 
a terminal sets the value of an option:

::

  paperbuilder config pandoc_scholar_path /path/to/pandoc/scholar

This command will set the ``paper_scholar_path`` option to ``/path/to/pandoc/scholar``.
The command is context-aware and will update the ``config.yaml`` file depending on the
path from which the command is executed. Thus if the above command is executed outside 
any paper project folder, the user configuration will be updated. However, if the command
is executed from the paper project root folder or from a paper version folder, then the
corresponding ``config.yaml`` files in those folders will be updated. To explicitly
indicate the level at which an option should be set, use one of the following commands:

::

  paperbuilder config --user KEY VALUE
  paperbuilder config --project KEY VALUE
  paperbuilder config --version VERSION KEY VALUE

where ``VERSION`` is the name of a paper version, ``KEY`` is the option to be configured and
``VALUE`` is the new value of the option. Note that you also may have to specify the project
path using the ``--path PATH`` argument, if the ``paperbuilder config`` command is executed
outside a paper project folder.

To unset a configuration option or to get the value of an existing configuration option, either
add the ``--unset`` or ``--get`` arguments to the command. For example:

::

  paperbuilder config --unset KEY
  paperbuilder --project --get KEY

To change a configuration file in a text editor, you can use the ``--edit`` argument. This will
launch the editor and open the appropriate configuration file. For example:

::

  paperbuilder config --user --edit

For this to work, you will first have to configure the executable of the editor. Note that just
like all other options you can configure the editor at each of the user, project and paper
version levels. Most often, however, you would set the editor at the user level:

::

  paperbuilder config --user editor gedit

