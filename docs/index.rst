Build scientific manuscripts
============================

PaperBuilder is a Python tool for building (scientific) manuscripts that include
text, figures, tables, citations, equations, etc. The main idea behind PaperBuilder
is that content and its structure (i.e. the text) is separated from presentation
(i.e. layout) and data (e.g. statistics). PaperBuilder is particularly useful when
the content of a manuscript (figures, tables, etc.) is dynamically generated 
and changes frequently. PaperBuilder is built on top of the document conversion tool 
`pandoc <https://pandoc.org/>`_ and the 
`pandoc-scholar <https://github.com/pandoc-scholar/pandoc-scholar>`_ extension for
scholarly documents.


.. toctree::
    :maxdepth: 2
    :caption: Contents:
    
    overview
    getting_started
    configuration
    how_to_write_paper
    how_to_create_analysis
    command_line_interface


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
