template = \
"""
---
title: "Paper title"
author:
  - John Johnson:
      institute: [a, b]
      email: john.johnson@example.com
      equal_contributor: True
  - Elizabeth Queen:
      institute: [a]
      email: elizabeth.queen@example.com
      equal_contributor: True
  - Jane Doe:
      institute: [b, c]
      email: jane.doe@example.com
  - Cherry Blossom:
      institute: [a, b, c]
      email: cherry.blossom@example.com
      address: Main Street 1001, Big City, Country
      correspondence: "yes"
institute:
  - a:
      name: Institute A
      address: Main Street 1001, Big City, Country
  - b: Department B, Institute B, City, Country
  - c: Institute C, City, Country
output: pdf_document
geometry: margin=2.0cm
linestretch: "1.0"
linenumbers: True
---

\doublespacing

# Keywords
Keyword 1; Keyword 2; Keyword 3

# Abstract
(to be written)

# Introduction
(to be written)

# Results
(to be written)

# Discussion
(to be written)

# Acknowledgments
(to be written)

# Author Contributions
(to be written)

# Declaration of Interests
The authors declare no competing interests.

# References
<div id="refs"></div>

# Methods
(to be written)

"""

analysis_spec = {
    'summary': 'code.summarize',
    'tables': {
        'function': 'code.tabulate',
        'args': {'n': 3},
    },
    'figures': {
        'main': {
            'layout': {
                'kind': 'grid',
                'ncols': 2,
                'nrows': 2,
                'group': 'columns',
            },
        },
    },
    'plots': {
        'col1': 'code.plot_column1',
        'col2': {
            'function': 'code.plot_column2',
            'args': {'npoints': 100}
        }
    }
}

plot_options = {
    'colors': {
        'group1': 'red',
        'group2': 'blue',
        'annotation': 'black',
    },
    'style_library': {
        'annotation': {'lines.linewidth': 1}
    },
    'style': [
        'seaborn-white', {'lines.linewidth': 3},
    ],
}

code = \
"""
import numpy as np
import matplotlib.style

def summarize():
    return {'key':'value', 'number':1}

def tabulate(n=1):
    return list([str(x) for x in range(n)]),  np.zeros((n,3))

def plot_column1(ax):
    ax['row1'].plot([0,1],[0,1], color='group1')
    with matplotlib.style.context('annotation'):
        ax['row1'].axhline(0.5, color='annotation')
    
    ax['row2'].plot([0,1],[1,0], color='group2')

def plot_column2(ax, npoints=10):
    xy = np.random.normal(size=(npoints,2))
    ax['row1'].plot(xy[:,0], xy[:,1], '.', color='group1')
    ax['row2'].plot(xy[:,1], xy[:,0], '.', color='group2')

"""
