import pathlib
import importlib
import re

import yaml
from jinja2 import Environment, FileSystemLoader, Template, Undefined

#from .paper import PaperVersion
from .utilities import dict_merge, map_nested_dicts_modify, add_path
from .plot_utilities import custom_color_context, custom_style_library_context

from collections import Mapping

import matplotlib
import matplotlib.pyplot as plt

import numpy as np
import sys

# install ipynb notebook loader (requires ipython)
try:
    from .notebook import install_notebook_loader
    install_notebook_loader(defs_only=True)
except:
    # ipython notebooks are not available for analysis
    pass

# preprocessing (jinja2 templating) functions
class CustomUndefined(Undefined):
    def __getattr__(self, attr):
        return CustomUndefined(name='.'.join([self._undefined_name, attr]))
    
    def __str__(self):
        return "**missing**: *{}*".format(self._undefined_name)

def load_variables(path, prefix=False):
    """Load all yaml files in path

    Parameters
    ----------
    path : str or list of str
        One or more paths to yaml files
    prefix : bool
        Prefixes variable keys in dictionary with the name of the
        yaml file. For example the variable c.d in file a.b.yaml,
        will be available as a.b.c.d in the final dictionary.
    
    Returns
    -------
    dict

    """

    if not isinstance(path, (list, tuple)):
        path = [path,]
    
    if len(path)==0:
        return {}

    # get all yaml files in paths
    var_files = []
    for x in path:
        var_files.extend(pathlib.Path(x).glob('*.yaml'))

    variables = []
    for f in var_files:

        # load yaml file
        with open(f) as fid:
            v = yaml.load(fid)
        
        # optionally, prefix with file name
        if prefix:
            for part in reversed(v.stem.split('.')):
                v = {part:v}
        
        variables.append(v)
    
    # merge all dictionaries into one
    variables = dict_merge(*variables)
    
    return variables

def preprocess(root, src, vars, filters, target, prefix=False):
    """process jinja2 templates

    Parameters
    ----------
    root : str or Path
        Root path for templates
    src : str or list of str
        Relative paths to template source files in root path
    vars : str or list of str
        paths to yaml variable files
    filters : list of str
        Fully specified import path for filter dictionary. For example,
        `my.module.filters`.
    target : str or list of str
        Paths to output files
    prefix : 
        Prefixes variables with the name of the yaml file.

    """

    # construct jinja2 environment
    env = Environment(
        loader=FileSystemLoader(str(root)),
        block_start_string = r'\BLOCK{',
        block_end_string = r'}',
        variable_start_string = r'<<*', #'\VAR{',
        variable_end_string = r'*>>', #'}',
        comment_start_string = r'\#{',
        comment_end_string = r'}',
        line_statement_prefix = r'%%',
        line_comment_prefix = r'%#',
        trim_blocks = True,
        autoescape = False,
        undefined = CustomUndefined
    )

    # install custom filters
    for f in filters:
        try:
            # import filters
            module, var = f.rsplit('.',1)
            f = getattr(importlib.import_module(module), var)
        except:
            raise ValueError("Could not import filters {}".format(str(f)))

        if not isinstance(f, Mapping):
            raise ValueError("Custom filters need to be stored in a dictionary.")
        
        # install filters in environment
        env.filters.update(f)

    # get variables
    vars = load_variables(vars, prefix)

    if not isinstance(src, (tuple,list)):
        src, target = [src,], [target,]

    for x,y in zip(src, target):
        template = env.get_template(str(x))
        stream = template.stream(vars)
        stream.enable_buffering(size=5)
        stream.dump(str(y), encoding='UTF-8')
    

def get_text_source_files(text_basename, text_path, output_path):
    """Retrieve text source and target files.

    Source files are all markdown (.md) files that start with `name`.
    Optionally, source files include a sequence number and short
    description, e.g. `name-00.md` or `name-01-intro.md`. The `name.md`
    source file will be listed first, followed by other source files
    ordered by their sequence number. If a `name.yaml` header file exists,
    it will be returned as the very first source file. 
    
    Parameters
    ----------
    text_basename : str
        Base name for source files.
    text_path : pathlib.Path
        Path to source files.
    output_path : pathlib.Path
        Output path for target files
    
    Returns
    -------
    src_files, target_files : list

    """

    src_files = []
    
    # find optional {name}.yaml header file
    src = text_path.joinpath(text_basename+'.yaml')
    if src.exists():
        src_files.append(src)
    
    # find all {name}*.md files
    src = text_path.glob('{name}*.md'.format(name=text_basename))
    # and filter out those that do not match pattern
    pattern = re.compile(text_basename+r"(-(?P<seq>\d+)(-.*)?)?.md")
    match = [(pattern.fullmatch(x.name), x) for x in src]
    # extract sequence number or -1 if file has no sequence number
    match = [(int(m['seq']) if m['seq'] else -1, p) for m,p in match if m]

    # add source files sorted by sequence number
    src_files.extend([x[1] for x in sorted(match)])

    target_files = [output_path.joinpath(x.name).with_suffix(".processed"+x.suffix) for x in src_files]

    return src_files, target_files


# functions for parsing analysis specifications
def _import_function(f):
    """Import function from python module.

    Parameters
    ----------
    f : str
        Fully qualified import path, e.g. `package.module.function`.
    
    Returns
    -------
    fcn : imported function
    file : module file

    """

    if not isinstance(f, str):
        raise ValueError('Expecting a fully qualified function name')
    
    module, _, fcn = f.rpartition('.')
    if len(module)==0:
        raise ValueError('No module found for {}'.format(f))
    
    module = importlib.import_module(module)
    
    return getattr(module,fcn), module.__file__

def parse_spec(spec, name):
    """Parse specification of function and args.

    Parameters
    ----------
    spec : str, dict or list of dict

    Returns
    -------
    list of dicts

    """

    # spec should be one of str, dict or list of dict
    # spec dicts should have at least a function key
    if isinstance(spec, str):
        spec = [{'function':spec}]
    elif isinstance(spec, Mapping):
        spec = [spec,]

    spec = [x for x in spec if isinstance(x,Mapping) and 'function' in x]

    for x in spec:
        # import summary function
        x['function'], x['module'] = _import_function(name+'.'+x['function'])

        if not 'name' in x:
            x['name'] = x['function'].__name__
        
        if not 'args' in x:
            x['args'] = {}
        
    return spec

def parse_fig_spec(path, spec, name):
    """Parse figure layout specification.

    Parameters
    ----------
    path : Path
        Path to analysis
    spec : dict
        Dictionary in which each entry is a figure specification

    Returns
    -------
    dict

    """

    # mapping {name: layout.svg, 
    #          name: {layout: str, style: str|list}
    #          name: {layout: {...}, ...}}

    if not isinstance(spec, Mapping):
        raise ValueError("Invalid figure specification.")
    
    def _get_layout_path(path, file):
        file = pathlib.Path(file).expanduser()
        if not file.is_absolute():
            file = path.joinpath(file).resolve()
        return file

    # internal function to parse single figure spec
    def _parse(key, val, path):
        
        if isinstance(val, str):
            # a string either defines a svg layout file
            # or a layout function
            if val.endswith('.svg'):
                val = {'layout': {'kind': 'svg', 'file': _get_layout_path(path, val)}}
            else:
                val = {'layout': {'kind': 'function', 'function': val}}
        elif not isinstance(val, Mapping):
            raise ValueError("Invalid figure specification: {}".format(key))
        else:
            # default layout is a single plot (1x1 grid)
            if not 'layout' in val:
                val['layout'] = {'kind': 'grid', 'nrows': 1, 'ncols': 1}
            elif isinstance(val['layout'], str):
                if val['layout'].endswith('.svg'):
                    val['layout'] = {'kind': 'svg', 'file': _get_layout_path(path, val['layout'])}
                else:
                    val['layout'] = {'kind': 'function', 'function': val['layout']}
            elif not isinstance(val['layout'], Mapping) or not 'kind' in val['layout']:
                raise ValueError("Invalid figure layout specification: {}".format(key))
        
        # build layout
        if val['layout']['kind'] == 'svg':
            val['layout'] = SVGLayout(**val['layout'])
        elif val['layout']['kind'] == 'grid':
            val['layout'] = SubplotsLayout(**val['layout'])
        elif val['layout']['kind'] == 'function':
            if 'function' in val['layout']:
                val['layout']['function'] = name+'.'+val['layout']['function']
            val['layout'] = FunctionLayout(**val['layout'])
        else:
            raise ValueError("Unknown layout type.")

        if not 'style' in val:
            val['style'] = []
        elif isinstance(val['style'], str):
            val['style'] = [val['style'],]

        if not 'args' in val:
            val['args'] = {}

        if not 'plot_args' in val:
            val['plot_args'] = {}

        return val

    # parse all figure specs
    spec = dict( [(key, _parse(key, value, path)) for key,value in spec.items()] )
    
    return spec

def parse_plot_spec(spec, name):
    """Parse plot specifications.

    Parameters
    ----------
    spec : dict
        Dictionary in which each entry is a plot specification

    Returns
    -------
    dict

    """

    # name: str
    # name: {function: str, args: {}, style: str|list}
    if not isinstance(spec, Mapping):
        raise ValueError("Invalid plot specification.")
    
    # internal function to parse single plot spec
    def _parse(key, val):
        
        if isinstance(val, str):
            # a string defines a plot function
            val = {'function': val}
        elif not isinstance(val, Mapping) or not 'function' in val:
            raise ValueError("Invalid plot specification: {}".format(key))
        
        # resolve function
        val['function'], val['module'] = _import_function(name+'.'+val['function'])

        # parse style and function arguments
        if not 'style' in val:
            val['style'] = []
        elif isinstance(val['style'], str):
            val['style'] = [val['style'],]
        
        if not 'args' in val:
            val['args'] = {}

        return val

    # parse all plot specs
    spec = dict( [(key, _parse(key, value)) for key,value in spec.items()] )

    return spec


# layout classes
class SVGLayout:
    def __init__(self, file=None, output_layer='output',
                 hide_layer='layout', **kwargs):
        
        self._layout = None

        try:
            import figurefirst as fifi
            self._fifi = fifi
        except ModuleNotFoundError:
            raise ModuleNotFoundError("Please install figurefirst if you would like to use svg layouts.")

        self._output_layer = output_layer
        self._hide_layer = [hide_layer,] if isinstance(hide_layer,str) else hide_layer

        self._file = file
        self._layout = self._fifi.FigureLayout(str(self._file), dpi=1200)
    
    def create_axes(self):
        self._layout.make_mplfigures()
        if len(self._layout.figures)==1 and 'none' in self._layout.figures:
            return self._layout.axes_groups['none']
        else:
            return self._layout.axes_groups
    
    @property
    def dependencies(self):
        return [self._file,]
    
    def save(self, target):
        # clear target layer
        self._layout.clear_fflayer(self._output_layer)

        # insert figures
        for key, fig in self._layout.figures.items():
            self._layout.append_figure_to_layer(fig, self._output_layer, cleartarget=False)
        
        # hide layers
        for layer in self._hide_layer:
            self._layout.set_layer_visibility(layer, vis=False)
        
        # save
        self._layout.write_svg(str(target))
    
    def destroy_axes(self):
        for key, fig in self._layout.figures.items():
            try:
                fig.figure.close()
            except:
                pass
    
    def __del__(self):
        if self._layout:
            self.destroy_axes()

class SubplotsLayout:
    def __init__(self, nrows=1, ncols=1, group=None, array=False,
                 group_label=None, label=None, args=None, **kwargs):
        
        self._figure = None
        
        if args is None:
            args = {}
        elif not isinstance(args, Mapping):
            raise ValueError("Expected args to be a dict.")
        self._extra_args = args
        self._extra_args['squeeze'] = False

        self._nrows, self._ncols = int(nrows), int(ncols)

        self._group = group # None or 'columns' or 'rows'
        self._group_label = group_label # None or list of str

        self._array = array
        self._label = label # str or list of str or list of list of str

        # no grouping, array==False
        #   all axes have a name, set by label property
        # no grouping, array==True
        #   axes returned as array under label
        # grouping, array==False
        #   groups set by group_label, axes have a name set by label property
        # grouping, array==True
        #   groups set by group_label, axes returned as array, label is ignored

    def create_axes(self):

        self._figure, axes = plt.subplots(nrows=self._nrows,
            ncols=self._ncols, **self._extra_args)

        N, M = self._nrows, self._ncols

        if self._group == 'columns' or self._group=='rows':
            
            labelN, labelM = 'row', 'col'
            if self._group == 'columns':
                N, M = M, N
                labelN, labelM = labelM, labelN

            if self._group_label is None or isinstance(self._group_label, str):
                prefix = labelN if self._group_label is None else self._group_label
                group_label = ['{}{}'.format(prefix,x+1) for x in range(N)]
            elif not isinstance(self._group_label, (list,tuple)) or len(self._group_label)!=N:
                raise ValueError('Invalid group names.')
            else:
                group_label = self._group_label
            
            if self._array:
                pass
            elif self._label is None or isinstance(self._label, str):
                prefix = labelM if self._label is None else self._label
                label = ['{}{}'.format(prefix,x+1) for x in range(M)]
            elif not isinstance(self._label, (list,tuple)) or len(self._label)!=M:
                raise ValueError('Invalid names.')
            else:
                label = self._label

            z = {}
            for i0,x in enumerate(group_label):
                if self._array:
                    z[x] = axes[:,i0] if self._group=='columns' else axes[i0,:]
                else:
                    z[x] = {}
                    for i1,y in enumerate(label):
                        if self._group == 'columns':
                            z[x][y] = axes[i1,i0]
                        else:
                            z[x][y] = axes[i0,i1]

        elif self._group is None:
            # ignore group labels
            
            if self._array:
                if self._label is None:
                    z['ax'] = axes
                elif isinstance(self._label, str):
                    z[self._label] = axes
                else:
                    raise ValueError("Label needs to be a string.")
                return z

            if self._label is None or isinstance(self._label, str):
                prefix = 'ax' if self._label is None else self._label
                label = ['{}{}'.format(prefix, x+1) for x in range(self._nrows*self._ncols)]
            elif not isinstance(self._label, (list,tuple)) or len(self._label)!=N*M:
                raise ValueError('Invalid names.')
            else:
                label = self._label
            
            #label = np.array(self._names, dtype=np.object).reshape((self._nrows, self._ncols))

            z = {x: ax for ax,x in zip(axes.ravel(), label)}

        else:
            raise ValueError("Invalid group type.")
        
        return z

    @property
    def dependencies(self):
        return []

    def save(self, target):
        if self._figure:
            self._figure.savefig(target)

    def destroy_axes(self):
        try:
            if self._figure:
                self._figure.close()
        except:
            pass
    
    def __del__(self):
        self.destroy_axes()

class FunctionLayout:
    def __init__(self, function=None, **kwargs):
        self._function, self._module = _import_function(function)
        self._kwargs = kwargs
        self._figure = None
    
    def create_axes(self):
        self._figure, ax = self._function(**self._kwargs)
        return ax
    
    @property
    def dependencies(self):
        return [self._module]

    def save(self, target):
        if self._figure:
            self._figure.saveas(target)
    
    def destroy_axes(self):
        try:
            if self._figure:
                self._figure.close()
        except:
            pass
    
    def __del__(self):
        self.destroy_axes()

# building analysis
def build_summary(spec, target):
    """Build a summary variable file.

    Parameters
    ----------
    spec : dict
        Summary function specification
    target : str or Path
        Path to output file

    Returns
    -------
    dict

    """

    # call summary function
    summary = spec['function'](**spec['args'])

    def _convert(x):
        if isinstance(x, np.ndarray):
            return x.tolist()
        elif isinstance(x, np.number):
            return np.asscalar(x)
        elif isinstance(x, (tuple, list)):
            return [_convert(y) for y in x]
        else:
            return x
    
    # convert numpy arrays
    map_nested_dicts_modify(summary, _convert)

    with open(target, 'w', encoding='UTF-8') as stream:
        yaml.dump(summary, stream)
    
    return summary

def build_table(spec, target):
    """Build a table csv file.

    Parameters
    ----------
    spec : dict
        Table function specification
    target : str or Path
        Path to output file
    
    """

    import csv
    
    # build table
    colnames, rows = spec['function'](**spec['args'])

    # save table to csv file
    with open(target, 'w') as csvfile:

        # rows is either a 2d array, a list/tuple of strings/numbers or a list/tuple of dicts
        if isinstance(rows, (list, tuple)) and all([isinstance(row,dict) for row in rows]):
            writer = csv.DictWriter(csvfile, fieldnames=colnames)
            writer.writeheader()
        else:
            writer = csv.writer(csvfile)
            writer.writerow(colnames)

        for row in rows:
            writer.writerow(row)

def build_figure(figure, plot_spec, plot_options, target):
    """Build a figure from layout and plots.

    Parameters
    ----------
    figure : dict
        Figure specification.
    plot_spec : dict
        Plot specifications
    plot_options : dict
        Plotting options
    target : str or Path
        Path to output file
    
    """

    axes = figure['layout'].create_axes()
    
    # install named color
    # install named styles
    # set default style + figure style
    style = plot_options.get('style', []) + figure.get('style', [])
    named_colors = plot_options.get('colors', {})
    named_styles = plot_options.get('style_library', {})
    
    with custom_color_context(named_colors), \
         custom_style_library_context(named_styles), \
         matplotlib.style.context(style):
        
        for plot, spec in plot_spec.items():
            
            ax = axes
            parts = plot.split('.')
            
            try:
                for p in parts:
                    ax = ax[p]
            except:
                # plot is not used in figure
                continue
            
            # build arguments dict
            args = dict()
            args.update(figure.get('args',{}))

            if 'plot_args' in figure:
                args.update(figure['plot_args'].get(plot,{}))

            args.update(spec.get('args',{}))

            # set plot style
            with matplotlib.style.context(spec.get('style', [])):
                # call plotting function
                spec['function'](ax, **args)

            # we have to make sure that named colors and styles
            # remain in place until we have destroyed the figure
            figure['layout'].save(target)
            figure['layout'].destroy_axes()

# generate DoIt tasks
def build_summary_tasks(spec, output_path, basename):

    tasks = []
    
    spec = parse_spec(spec, basename)

    for summary in spec:
        
        target = output_path.joinpath('{}.{}.yaml'.format(basename, summary['name']))

        tasks.append({
            'actions': [(build_summary, [summary, target], {})],
            'targets': [target],
            'file_dep': [summary['module']],
            'basename': '{}-{}'.format('analysis', basename),
            'name': '{}-{}'.format('summary', summary['name']),
            'clean': True,
        })

    return tasks

def build_table_tasks(spec, output_path, basename):

    tasks = []
    
    spec = parse_spec(spec, basename)

    for table in spec:
        
        target = output_path.joinpath('{}.{}.csv'.format(basename, table['name']))

        tasks.append({
            'actions': [(build_table, [table, target], {})],
            'targets': [target],
            'file_dep': [table['module']],
            'basename': '{}-{}'.format('analysis', basename),
            'name': '{}-{}'.format('table', table['name']),
            'clean': True,
        })
    
    return tasks

def build_figure_tasks(input_path, fig_spec, plot_spec, options, output_path, basename):
    
    tasks = []

    fig_spec = parse_fig_spec(input_path, fig_spec, basename)
    plot_spec = parse_plot_spec(plot_spec, basename)

    for name, figure in fig_spec.items():
        
        target = output_path.joinpath('{}.{}.svg'.format(basename, name))

        dependencies = figure['layout'].dependencies
        # TODO: add fig_spec/plot_spec file dependency
        # TODO: add plot module dependencies

        tasks.append({
            'actions': [(build_figure, [figure, plot_spec, options, target], {})],
            'targets': [str(target)],
            'file_dep': dependencies,
            'basename': '{}-{}'.format('analysis', basename),
            'name': '{}-{}'.format('figure', name),
            'clean': True,
        })
    
    return tasks

def build_preprocess_task(text_basename, text_path, variable_path, output_path, filters):

    variable_files = variable_path.glob('*.yaml')

    src_files, target_files = get_text_source_files(text_basename, text_path, output_path)
    rel_src_files = [x.relative_to(text_path) for x in src_files]

    task = {
        'actions': [(preprocess, [text_path, rel_src_files, variable_path, 
                        filters, target_files],{})],
        'targets': target_files,
        'file_dep': src_files + list(variable_files),
        'clean': True,
        'basename': 'paper',
        'name': 'preprocess',
    }

    return [task,]

def build_json_task(text_basename, text_path, output_path, pandoc_scholar_path=None):
    
    filters = []

    if pandoc_scholar_path:   
        filters = [
            pandoc_scholar_path.joinpath('lua-filters', 'cito', 'cito.lua'),
            pandoc_scholar_path.joinpath('lua-filters', 'abstract-to-meta', 'abstract-to-meta.lua'),
            pandoc_scholar_path.joinpath('lua-filters', 'scholarly-metadata', 'scholarly-metadata.lua'),
        ]

    src_files, target_files = get_text_source_files(text_basename, text_path, output_path)

    target = output_path.joinpath(text_basename + ".enriched.json")

    cmd = "pandoc {filters} --to=json --output={output} {targets}".format(
        filters = " ".join(["--lua-filter={}".format(x) for x in filters]),
        targets = " ".join([str(x) for x in target_files]),
        output = target
    )

    task = {
        'actions': [cmd],
        'targets': [target],
        'file_dep': target_files,
        'clean': True,
        'basename': 'paper',
        'name': 'json',
    }

    return [task,]

def build_pdf_task(text_basename, output_path, pandoc_scholar_path=None, csl_file=None, latex_template=None, bibliography=None):

    filters = ['pantable', 'pandoc-fignos', 'pandoc-eqnos', 'pandoc-tablenos']
    writer = "--standalone {filters}".format(
        filters = " ".join(["--filter={}".format(x) for x in filters]))

    if csl_file:
        writer += " --csl={csl}".format(csl=csl_file)

    filters = []
    
    if pandoc_scholar_path:
        filters = [
            pandoc_scholar_path.joinpath('scholar-filters', 'template-helper.lua')
        ]
        if not latex_template:
            latex_template = pandoc_scholar_path.joinpath('templates', 'pandoc-scholar.latex')
    
    pdf_options = "--pdf-engine=xelatex"

    if latex_template:
        pdf_options += " --template={}".format(latex_template)
    
    if bibliography:
        pdf_options += " --bibliography={}".format(bibliography)
    
    target = output_path.joinpath(text_basename + ".pdf")
    src = output_path.joinpath(text_basename + ".enriched.json")

    cmd = "pandoc {writer} {pdf} {filters} --citeproc --resource-path={resource} --output={target} {src}".format(
        writer = writer,
        pdf = pdf_options,
        filters = " ".join(["--lua-filter={}".format(x) for x in filters]),
        target = target,
        src = src,
        resource = output_path.parent
    )

    task = {
        'actions': [cmd],
        'targets': [str(target)],
        'file_dep': [str(src)],
        'clean': True,
        'basename': 'paper',
        'name': 'pdf',
    }
    
    return [task,]

def build_docx_task(text_basename, output_path, pandoc_scholar_path=None, csl_file=None, reference=None, bibliography=None):

    filters = ['pantable', 'pandoc-fignos', 'pandoc-eqnos', 'pandoc-tablenos']
    writer = "--standalone {filters}".format(
        filters = " ".join(["--filter={}".format(x) for x in filters]))

    if csl_file:
        writer += " --csl={csl}".format(csl=csl_file)

    if bibliography:
        writer += " --bibliography={}".format(bibliography)

    if pandoc_scholar_path:
        filters = [
            pandoc_scholar_path.joinpath('lua-filters', 'author-info-blocks', 'author-info-blocks.lua')
        ]
    else:
        filters = []

    target = output_path.joinpath(text_basename + ".docx")
    src = output_path.joinpath(text_basename + ".enriched.json")

    cmd = "pandoc {writer} {docx} {filters} --citeproc --output={target} {src}".format(
        writer = writer,
        docx = "" if reference is None else "--reference-docx={}".format(reference), 
        filters = " ".join(["--lua-filter={}".format(x)for x in filters]),
        target = target,
        src = src
    )

    task = {
        'actions': [cmd],
        'targets': [str(target)],
        'file_dep': [str(src)],
        'clean': True,
        'basename': 'paper',
        'name': 'docx',
    }

    return [task,]

def build_image_task(figpath, dpi=300, bg='rgb(255,255,255)', opacity=1.):
    
    tasks = []
    svgfiles = figpath.glob('*.svg')

    cmd = 'inkscape {} -C --export-png={} --export-dpi={dpi} --export-background="{bg}" --export-background-opacity={opacity}'
    
    for src in svgfiles:
        target = src.with_suffix('.png')
        tasks.append({
            'actions': [cmd.format(src, target, dpi=dpi, bg=bg, opacity=opacity),],
            'targets': [target,],
            'file_dep': [src,],
            'clean': True,
            'basename': 'image',
            'name': src.stem,
        })
    
    return tasks



# def task_paper(path='.'):
#     tasks = build_paper_tasks(path)
#     for task in tasks:
#         yield task

# def task_analysis(path='.'):
#     paper = PaperVersion(path)
    
#     for name, analysis in paper.analysis.items():
#         tasks = build_analysis_tasks(analysis)
#         for task in tasks:
#             yield task

# def get_task_creators(path='.'):
#     from functools import partial
#     return {'task_paper': partial(task_paper, path=path), 
#             'task_analysis': partial(task_analysis, path=path)}


# def build_paper_tasks(path):
#     """Construct tasks for building paper.

#     Parameters
#     ----------
#     path : str
#         Path to paper project or paper version
    
#     Returns
#     -------
#     list of doit task descriptions

#     """

#     paper = PaperVersion(path)

#     name = paper.config['text_basename']

#     # find and combine all preprocessing filters
#     # always add built-in filters
#     # filters are ordered, so that higher configuration levels
#     # (e.g. paper version) may override lower configuration levels
#     preprocess_filters = ['paperbuilder.filters.FILTERS',]
#     for cfg in reversed(paper.config.maps):
#         #preprocess_filters |= set(cfg.get('filters', []))
#         preprocess_filters.extend(cfg.get('filters', []))
    

#     tasks = []

#     # PREPROCESS task
#     variable_path = paper.path.joinpath('variables')
#     variable_files = variable_path.glob('*.yaml')

#     text_path = paper.path.joinpath('text')
#     src_files = get_text_source_files(text_path, name)
#     src_files = [x.relative_to(paper.path) for x in src_files]

#     output_path = paper.path.joinpath('output')
#     target_files = [ output_path.joinpath(x.name).with_suffix(".processed"+x.suffix) for x in src_files ]
#     target_files = [x.relative_to(paper.path) for x in target_files]

#     tasks.append({
#         'actions': [(preprocess, [src_files, variable_path, 
#                      preprocess_filters, target_files],{})],
#         'targets': target_files,
#         'file_dep': src_files + list(variable_files),
#         'clean': True,
#         'basename': 'paper',
#         'name': 'preprocess',
#     })

#     # PANDOC JSON task
#     pandoc_scholar = False
#     filters = []

#     pandoc_scholar_path = paper.config.get('pandoc_scholar_path', None)

#     if not pandoc_scholar_path is None and pandoc_scholar_path!='':
#         pandoc_scholar_path = pathlib.Path(pandoc_scholar_path)
#         pandoc_scholar = True
        
#         filters = [
#             pandoc_scholar_path.joinpath('lua-filters', 'cito', 'cito.lua'),
#             pandoc_scholar_path.joinpath('lua-filters', 'abstract-to-meta', 'abstract-to-meta.lua'),
#             pandoc_scholar_path.joinpath('lua-filters', 'scholarly-metadata', 'scholarly-metadata.lua'),
#         ]

#     cmd = "pandoc {filters} --to=json --output=output/{name}.enriched.json {targets}".format(
#         filters = " ".join(["--lua-filter={}".format(x) for x in filters]),
#         name = name,
#         targets = " ".join([str(x) for x in target_files]),
#     )

#     tasks.append({
#         'actions': [cmd],
#         'targets': ['output/{}.enriched.json'.format(name)],
#         'file_dep': target_files,
#         'clean': True,
#         'basename': 'paper',
#         'name': 'json',
#     })

#     # PANDOC PDF task
#     csl_path = paper.config.get('csl_path', None)
#     csl_file = paper.config.get('csl_file', None)

#     filters = ['pantable', 'pandoc-fignos', 'pandoc-eqnos', 'pandoc-tablenos', 'pandoc-citeproc']
#     writer = "--standalone {filters}".format(
#         filters = " ".join(["--filter={}".format(x) for x in filters]))
    
#     if not csl_path is None and not csl_file is None:
#         writer += " --csl={csl}".format(csl = csl_path.joinpath(csl_file))

#     if pandoc_scholar:
#         filters = [
#             pandoc_scholar_path.joinpath('scholar-filters', 'template-helper.lua')
#         ]
#         default_latex_template = pandoc_scholar_path.joinpath('templates', 'pandoc-scholar.latex')
#     else:
#         filters = []
#         default_latex_template = None
    
#     latex_template = paper.config.get('latex_template', default_latex_template)
#     pdf_options = "--pdf-engine=xelatex"

#     if latex_template:
#         pdf_options += " --template={}".format(latex_template)

#     cmd = "pandoc {writer} {pdf} {filters} --output=output/{name}.pdf output/{name}.enriched.json".format(
#         writer = writer,
#         pdf = pdf_options,
#         filters = " ".join(["--lua-filter={}".format(x) for x in filters]),
#         name = name,
#     )

#     tasks.append({
#         'actions': [cmd],
#         'targets': ['output/{}.pdf'.format(name)],
#         'file_dep': ['output/{}.enriched.json'.format(name)],
#         'clean': True,
#         'basename': 'paper',
#         'name': 'pdf',
#     })

#     # PANDOC DOCX task
#     if pandoc_scholar:
#         filters = [
#             pandoc_scholar_path.joinpath('lua-filters', 'author-info-blocks', 'author-info-blocks.lua')
#         ]
#     else:
#         filters = []

#     reference = paper.config.get('docx_reference', None)

#     cmd = "pandoc {writer} {docx} {filters} --output=output/{name}.docx output/{name}.enriched.json".format(
#         writer = writer,
#         docx = "" if reference is None else "--reference-docx={}".format(docx_reference), 
#         filters = " ".join(["--lua-filter={}".format(x)for x in filters]),
#         name = name,
#     )

#     tasks.append({
#         'actions': [cmd],
#         'targets': ['output/{}.docx'.format(name)],
#         'file_dep': ['output/{}.enriched.json'.format(name)],
#         'clean': True,
#         'basename': 'paper',
#         'name': 'docx',
#     })

#     # FIGURE PNG task
#     figpath = paper.path.joinpath('figures')
#     svgfiles = figpath.glob('*.svg')

#     cmd = 'inkscape {} -C --export-png={} --export-dpi={dpi} --export-background="{bg}" --export-background-opacity={opacity}'
#     dpi = paper.config.get('image_dpi', 300)
#     bg = paper.config.get('image_background', 'rgb(255,255,255)')
#     opacity = paper.config.get('image_opacity', 1.0)

#     for src in svgfiles:
#         target = src.with_suffix('.png')
#         tasks.append({
#             'actions': [cmd.format(src, target, dpi=dpi, bg=bg, opacity=opacity),],
#             'targets': [target,],
#             'file_dep': [src,],
#             'clean': True,
#             'basename': 'image',
#             'name': src.stem,
#         })

#     return tasks

# def build_analysis_tasks(analysis):
#     """Construct tasks for analyses.

#     Parameters
#     ----------
#     analysis : PaperAnalysis object
    
#     Returns
#     -------
#     list of doit task descriptions

#     """

#     tasks = []

#     spec = analysis.load_spec()
#     plot_options = analysis.load_plot_options()
    
#     # convert colors to hex
#     plot_options['colors'] = {k:matplotlib.colors.to_hex(v) for k,v in plot_options.get('colors',{}).items()} 

#     varpath = analysis.paper.path.joinpath('variables')
#     tablepath = analysis.paper.path.joinpath('tables')
#     figurepath = analysis.paper.path.joinpath('figures')

#     with add_path(analysis.path):
        
#         # SUMMARY TASKS
#         summary_spec = parse_spec(spec.get('summary', [])) #, analysis.name)

#         for summary in summary_spec:

#             # get path to module that contains summary function
#             target = varpath.joinpath('{}.{}.yaml'.format(analysis.name, summary['name']))

#             tasks.append({
#                 'actions': [(build_summary, [summary, target], {})],
#                 'targets': [target],
#                 'file_dep': [summary['module']],
#                 'basename': '{}-{}'.format('analysis', analysis.name),
#                 'name': '{}-{}'.format('summary',summary['name']),
#             })
        
#         # TABLE TASKS
#         table_spec = parse_spec(spec.get('tables', [])) #, analysis.name)

#         for table in table_spec:

#             target = tablepath.joinpath('{}.{}.csv'.format(analysis.name, table['name']))

#             tasks.append({
#                 'actions': [(build_table, [table, target], {})],
#                 'targets': [target],
#                 'file_dep': [table['module']],
#                 'basename': '{}-{}'.format('analysis', analysis.name),
#                 'name': '{}-{}'.format('table', table['name']),
#             })
        
#         # FIGURE TASKS
#         fig_spec = parse_fig_spec(spec.get('figures',{}))
#         plot_spec = parse_plot_spec(spec.get('plots',{})) #, analysis.name)

#         for name, figure in fig_spec.items():
            
#             target = figurepath.joinpath('{}.{}.svg'.format(analysis.name, name))
#             # TODO: figure dependencies

#             tasks.append({
#                 'actions': [(build_figure, [figure, plot_spec, plot_options, target], {})],
#                 'targets': [target],
#                 #'file_dep': [], #[figure['module']], # TODO: resolve dependencies in parse fcn, which requires loading svg layout file
#                 'basename': '{}-{}'.format('analysis', analysis.name),
#                 'name': '{}-{}'.format('figure', name),
#             })



#     return tasks
