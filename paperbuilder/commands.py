import pathlib
import subprocess

from paperbuilder.paper import PaperProject, PaperVersion, UserConfig

def paper_init(path='.', name=None, description=None,
               version='draft', package=None, force=False, create=False, **kwargs):

    PaperProject.new_project(path=path, name=name, force=force, create=create,
        description=description, version=version, package=package)

def paper_create(name=None, path='.', clone=None, current=False, **kwargs):
    
    project = PaperProject(path=path, lookup=True)
    project.create_version(name=name, clone=clone, current=current)

def paper_remove(name=None, path='.', **kwargs):

    project = PaperProject(path=path, lookup=True)
    project.remove_version(name=name)

def paper_config(key=None, value=None, target=None, action='set', 
    path='.', **kwargs):
    
    if target==':user:':
        cfg = UserConfig()
    elif target==':project:':
        project = PaperProject(path=path, lookup=True)
        cfg = project.config
    elif target is None:
        path = pathlib.Path(path).expanduser().resolve()
        project_path = PaperProject._lookup_paper_project(path)
        if project_path is None:
            cfg = UserConfig()
        else:
            relpath = path.relative_to(project_path).parts
            if len(relpath)>1 and relpath[0]=='versions':
                version = PaperVersion(project_path, relpath[1])
                cfg = version.config
            else:
                project = PaperProject(path=path, lookup=True)
                cfg = project.config
    else:
        version = PaperVersion(project=path, version=target)
        cfg = version.config
    
    if action=='unset':
        if key is None:
            raise ValueError('missing key')
        del cfg[key]
        cfg.save()
    elif action=='get':
        if key is None:
            raise ValueError('missing key')
        print('{}={}'.format(key,cfg[key]))
    elif action=='set':
        if key is None or value is None:
            raise ValueError('missing key and/or value')
        cfg[key] = value
        cfg.save()
    elif action=='list':
        if hasattr(cfg, 'maps'):
            cfg = cfg.maps[0]

        for k,v in cfg.items():
            print("{} = {}".format(k,v))
    elif action=='merged':
        # TODO: lists of filters option should be merged across config maps
        for k,v in cfg.items():
            print("{} = {}".format(k,v))
    elif action=='edit':
        editor = cfg.get('editor', None)
        if editor is None:
            print("No editor configured.")
        subprocess.call("{} {} &".format(editor, str(cfg.path)), shell=True)
    else:
        raise ValueError('unknown config action')

def paper_build(target=None, version=None, force=False, path='.', complete=False, **kwargs):
    
    v = PaperVersion(project=path, version=version)
    
    if complete:
        v.build_analysis(force=force)
    
    v.build_images(force=force)
    v.build(target=target, force=force)
    

def paper_preprocess(version=None, force=False, path='.', **kwargs):
    
    v = PaperVersion(project=path, version=version)
    v.preprocess(force=force)

def paper_info(version=None, path='.', **kwargs):
    # TODO: implement info method on PaperVersion/PaperProject classes
    v = PaperVersion(project=path, version=version)
    print("project: ?")
    print("paper: ?")

def paper_clean(target=None, version=None, complete=False, path='.', **kwargs):
    
    v = PaperVersion(project=path, version=version)
    
    v.clean(target=target)
    
    if complete:
        v.clean_images()
        v.clean_analysis()


def analysis_create(name=None, clone=None, version=None, path='.', **kwargs):
    
    v = PaperVersion(path, version)
    v.create_analysis(name=name, clone=clone)

# analysis remove [NAME [NAME ...]] [--all] [--version VERSION] [--clean] [--path PATH]
def analysis_remove(name=None, version=None, complete=False, clean=True, path='.', **kwargs):

    v = PaperVersion(path, version)
    v.remove_analysis(name=name, clean=clean, complete=complete)

# analysis info NAME [--version VERSION]
def analysis_info(name=None, version=None, **kwargs):
    # TODO: implement
    # get paper version path
    # if no name specified, show info for all analyses
    pass

# analysis build [NAME [NAME ...]] [--all] [--force] [--version VERSION] [--path PATH] [--target TARGET]
def analysis_build(name=None, version=None, complete=False, force=False,
                   target=None, path='.', **kwargs):
    
    v = PaperVersion(project=path, version=version)
    v.build_analysis(name=name, complete=complete, force=force, target=target)

    
# analysis clean [NAME [NAME ...]] [--all] [--version VERSION] [--path PATH] [--target TARGET]
def analysis_clean(name=None, version=None, complete=False,
                   target=None, path='.', **kwargs):

    v = PaperVersion(project=path, version=version)
    v.clean_analysis(name=name, complete=complete, target=target)


commands = {
    'init': paper_init,
    'create': paper_create,
    'remove': paper_remove,
    'config': paper_config,
    'build': paper_build,
    'preprocess': paper_preprocess,
    'info': paper_info,
    'clean': paper_clean,
    'analysis': {
        'create': analysis_create,
        'remove': analysis_remove,
        'info': analysis_info,
        'build': analysis_build,
        'clean': analysis_clean,
    }
}

