"""paperbuilder - Build scientific papers"""

from .utilities import package_info

__version__ = package_info()['version']
__author__ = 'Fabian Kloosterman <fabian.kloosterman@nerf.be>'
__all__ = []
