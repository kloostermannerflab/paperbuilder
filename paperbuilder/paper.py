from collections.abc import Mapping, MutableMapping
from collections import ChainMap

import datetime
import shutil
import pathlib

import yaml

from .utilities import dict_merge, get_config_path, add_path, set_cwd, package_info
from .build import build_summary_tasks, build_table_tasks, build_figure_tasks
from .build import build_preprocess_task, build_json_task, build_pdf_task
from .build import build_docx_task, build_image_task

import multiprocessing

default_config = {
    #'pandoc_scholar_path': None,
    #'csl_path': None,
    #'csl_file': None,
    #'bibliography_path': None,
    #'bibliography_file': None,
    #'latex_template': None,
    #'docx_reference': None,
    #'odt_reference': None,
    'build_target': ['pdf', 'docx'],
    'text_basename': 'manuscript',
    #'text_template_path': None,
    'text_template': 'default',
}

class YamlConfig(MutableMapping):
    def __init__(self, path, filename='config'):
        
        self._path = pathlib.Path(path).expanduser().resolve()
        self._file = self._path.joinpath(filename+'.yaml')
        self._config = self._load(self._file)

    @property
    def path(self):
        return self._file

    def _load(self, path):
        cfg = {}
        
        if path.exists():
            with path.open(mode='r') as stream:
                cfg = yaml.safe_load(stream)
            if cfg is None:
                cfg = {}
        
        return cfg
    
    def _save(self, path, cfg):
        
        if not path.parent.exists():
            path.parent.mkdir(parents=True)

        if path.exists():
            # back up
            shutil.copyfile(path, str(path)+".old")

        with path.open(mode='w') as stream:
            yaml.dump(cfg, stream)

    def reload(self):
        self._config = self._load(self._file)
    
    def save(self, to=None):
        if to is None:
            to = self._file
        else:
            to = pathlib.Path(to).expanduser().resolve()
            if to.is_dir():
                to = to.joinpath('{}.yaml'.format(self._filename))

        self._save(to, self._config)
        
    def __getitem__(self, key):
        return self._config.__getitem__(key)
    
    def __len__(self):
        return self._config.__len__()
    
    def __iter__(self):
        return self._config.__iter__()
    
    def __setitem__(self, key, value):
        self._config.__setitem__(key, value)
    
    def __delitem__(self, key):
        self._config.__delitem__(key)

class UserConfig(YamlConfig):
    def __init__(self, **kwargs):
        super().__init__(path=get_config_path(), **kwargs)

class ChainedConfig(ChainMap):
    
    @property
    def path(self):
        return self.maps[0].path

    def save(self, to=None):
        try:
            self.maps[0].save(to=to)
        except AttributeError:
            pass
    
    def reload(self):
        for m in self.maps:
            try:
                m.reload()
            except AttributeError:
                pass

def _relocate(src, path, base):
    
    parts = src.stem.split('-', 1)

    if len(parts)==1:
        stem = base
    else:
        stem = "{}-{}".format(base, parts[1])

    target = path.joinpath(stem + src.suffix)

    return target

def execute_tasks(path, task_creators, cmd='run', subprocess=False, options=None, force=False):
    from doit.cmd_base import ModuleTaskLoader
    from doit.doit_cmd import DoitMain

    args = [cmd, '--backend=sqlite3', '--db-file=build.db'] #, '--dir={}'.format(str(path))]
    
    if force:
        args += ['-a']
    
    if options:
        args += options
    
    if subprocess:
        p = multiprocessing.Process(target=execute_tasks, args=(path, task_creators,), 
                kwargs={'cmd':cmd, 'options':options, 'subprocess':False, 'force': force})
        p.start()
        p.join()
        return p.exitcode
    else:
        with set_cwd(path):
            return DoitMain(ModuleTaskLoader(task_creators)).run(args)

class PaperProject():

    class _versions_helper(Mapping):
        def __init__(self, project):
            self._project = project
        def current(self):
            return self.__getitem__(None)
        def __getitem__(self, key):
            if key is None:
                key = self._project.current_version
                if key is None:
                    raise KeyError('No default version set.')
            return PaperVersion(self._project, key)
        def __len__(self):
            # get number of paper versions
            path = self._project.path.joinpath('versions')
            versions = [child.stem for child in path.iterdir() if child.is_dir()]
            return len(versions)
        def __iter__(self):
            # iterator
            path = self._project.path.joinpath('versions')
            versions = [child.stem for child in path.iterdir() if child.is_dir()]
            for version in versions:
                yield version
    
    def __init__(self, path='.', lookup=False):
        
        path = pathlib.Path(path).expanduser().resolve()

        if not PaperProject._is_paper_project(path):
            if lookup:
                path = PaperProject._lookup_paper_project(path)
                if path is None:
                    raise ValueError('Could not find paper project')
            else:
                raise ValueError('Path does not contain paper project')

        self._path = path
        self._versions = PaperProject._versions_helper(self)
        
        self._config = ChainedConfig(YamlConfig(path), UserConfig(), default_config)
    
    @property
    def path(self):
        return self._path
    
    @property
    def config(self):
        return self._config

    @property
    def current_version(self):
        return self.config.get('current', None)

    @staticmethod
    def _is_paper_project(path):
        if path.is_dir() and path.joinpath('paper.project').exists():
            return True
        
        return False

    @staticmethod
    def _lookup_paper_project(path):
         
        if PaperProject._is_paper_project(path):
            return path

        parent = path.parent

        if parent==path:
            return None
        else:
            return PaperProject._lookup_paper_project(parent)

    @classmethod
    def new_project(cls, path, name=None, description=None,
                    version='draft', create=False, force=False, package=None):
        
        # path is the full path to the paper project folder
        # if path does not exist, an exception is thrown
        # unless create=True
        # if name=None, then the stem of the path will be taken as the project name

        path = pathlib.Path(path).expanduser().resolve()

        if not path.exists():
            if create:
                path.mkdir()
            else:
                raise ValueError("Project path {} does not exist. Create path or use --create option".format(str(path)))
        elif len(list(path.glob('*')))>0 and not force:
            raise ValueError("Path {} is not empty. Cannot create paper project. Use --force to override".format(str(path)))
        elif path.joinpath('project.info').exists():
            raise ValueError("Path {} already contains paper project.".format(str(path)))
        
        if name is None:
            name = path.stem

        if description is None:
            description = "This is a paper project."
        
        if package is None:
            package = "papers.{}".format(name.replace('.', '_'))


        info = {
            'name': name,
            'description': description,
            'package': package,
            'created': datetime.datetime.now().isoformat(),
            'version': package_info()['version'],
        }
        with path.joinpath('paper.project').open(mode='w') as stream:
            yaml.dump(info, stream)

        # create paper versions folder
        versions_path = path.joinpath('versions')
        versions_path.mkdir()

        # create configuration file
        cfg = {}
        with path.joinpath('config.yaml').open(mode='w') as stream:
            yaml.dump(cfg, stream)
        
        # create templates folder
        templates_path = path.joinpath('templates', 'default')
        templates_path.mkdir(parents=True)
        # copy internal default template
        from .template import template
        templates_path.joinpath('default.md').write_text(template)

        # set up python package
        codepath = path.joinpath('code', *package.split('.'))
        codepath.mkdir(parents=True)
        codepath.joinpath('__init__.py').touch()
        
        project = cls(path)

        if not version is None:
            project.create_version(version, current=True)
        
        return project

    def create_version(self, name=None, clone=None, current=False):
        
        version_path = self.path.joinpath('versions', name)

        if version_path.exists():
            raise ValueError('Paper version {} already exists.'.format(name))
        
        if not clone is None:
            # clone is one of:
            # name -> existing version in current project
            # /path/to/project:name -> version in other project
            if ':' in clone:
                clone_project, clone_version = clone.split(':')
            else:
                clone_project, clone_version = self.path, clone
            
            clone_path = pathlib.Path(clone_project).joinpath('versions', clone_version)
            if not clone_path.exists():
                raise ValueError('Cannot clone non-existing paper version {}.'.format(clone))
            shutil.copytree(clone_path, version_path)
        else:
            version_path.mkdir()
            for target in ['output', 'analysis', 'figures', 'tables', 'variables', 'text']:
                version_path.joinpath(target).mkdir()
            
            # create config files
            version_path.joinpath('config.yaml').touch()
            version_path.joinpath('plot_options.yaml').touch()

            # create paper.md from template
            base_name = self.config['text_basename']
            template_path = self.config.get('text_template_path', None)
            template = self.config.get('text_template', None)

            # template can be one of:
            #    folder with yaml/md files
            #    md file
            # template can be absolute or relative to template path

            if template:
                template = pathlib.Path(template)
                if not template.is_absolute():
                    # check if template exists locally
                    p = self.path.joinpath('templates', template)
                    
                    # otherwise check in template path
                    if not p.exists():
                        if template_path:
                            p = pathlib.Path(template_path).joinpath(template)
                            template = p if p.exists() else None
                        else:
                            template = None
                    else:
                        template = p
                
                if not template or not template.exists():
                    print("Warning: could not find text template.")
                else:
                    if template.is_dir():
                        # get all files in folder
                        src = list(template.glob('*'))
                        target = [_relocate(x, version_path.joinpath('text'), base_name) for x in src]
                        # copy
                        for _from, _to in zip(src, target):
                            print(_from, _to)
                            shutil.copyfile(_from, _to)
                    else:
                        target = _relocate(template, version_path.joinpath('text'), base_name)
                        shutil.copyfile(template, target)
            else:
                # create file from internal template
                from .template import template
                version_path.joinpath('text', base_name + '.md').write_text(template)

        if current:
            self.config['current'] = name
            self.config.save()

    def remove_version(self, name=None):
        
        v = self.versions[name]
        shutil.rmtree(v.path)
        
        if 'current' in self.config and self.config['current']==name:
            try:
                del self.config['current']
                self.config.save()
            except:
                # `current` key not in first config
                pass

    @property
    def versions(self):
        return self._versions

class PaperVersion():

    class _analysis_helper(Mapping):
        def __init__(self, version):
            self._version = version
        def _analysis_list(self):
            path = self._version.path.joinpath('analysis')
            analyses = [child.stem for child in path.iterdir() if child.is_dir()]
            return analyses
        def __getitem__(self, key):
            if not key in self._analysis_list():
                raise KeyError('Analysis {} does not exist.'.format(key))
            return PaperAnalysis(self._version, key)
        def __len__(self):
            return len(self._analysis_list())
        def __iter__(self):
            for analysis in self._analysis_list():
                yield analysis

    def __init__(self, project, version=None):
        # project is either PaperProject or str
        # version is str or None

        if isinstance(project, PaperProject):
            self._project = project
        else:
            project = pathlib.Path(project).expanduser().resolve()
            self._project = PaperProject(project, lookup=True)
            
            rel_path = project.relative_to(self._project.path).parts
            if (len(rel_path)>1 and rel_path[0]=='versions' and 
                version is None and project.is_dir()):
                version = rel_path[1]
        
        if version is None:
            version = self._project.current_version

        if version is None or not version in list(self._project.versions.keys()):
            raise ValueError("No paper version {} found.".format(version))
        
        self._version = version
        self._analysis = PaperVersion._analysis_helper(self)
        self._config = self._project._config.new_child(
            YamlConfig(self._project.path.joinpath('versions', self._version)))
    
    @property
    def project(self):
        return self._project
    
    @property
    def config(self):
        return self._config

    @property
    def path(self):
        return self._project.path.joinpath('versions', self._version)
    
    @property
    def analysis(self):
        return self._analysis

    def build_tasks(self):
        
        name = self.config['text_basename']
        text_path = self.path.joinpath('text')
        variable_path = self.path.joinpath('variables')
        output_path = self.path.joinpath('output')

        # PREPROCESS task
        # find and combine all preprocessing filters
        # always add built-in filters
        # filters are ordered, so that higher configuration levels
        # (e.g. paper version) may override lower configuration levels
        preprocess_filters = ['paperbuilder.filters.FILTERS',]
        for cfg in reversed(self.config.maps):
            #preprocess_filters |= set(cfg.get('filters', []))
            preprocess_filters.extend(cfg.get('filters', []))

        preprocess_tasks = build_preprocess_task(name, text_path, 
            variable_path, output_path, preprocess_filters)

        # PANDOC JSON task
        pandoc_scholar_path = self.config.get('pandoc_scholar_path', None)
        if pandoc_scholar_path=='':
            pandoc_scholar_path = None
        elif not pandoc_scholar_path is None:
            pandoc_scholar_path = pathlib.Path(pandoc_scholar_path)
        
        json_tasks = build_json_task(name, text_path, output_path, pandoc_scholar_path)
        

        # PANDOC PDF task
        csl_path = self.config.get('csl_path', None)
        csl_file = self.config.get('csl_file', None)

        if not csl_path is None and not csl_file is None:
            csl_file = pathlib.Path(csl_path).joinpath(csl_file)

        latex_template = self.config.get('latex_template', None)
        
        bib_path = self.config.get('bibliography_path', None)
        bib_file = self.config.get('bibliography_file', None)

        if not bib_path is None and not bib_file is None:
            bib_file = pathlib.Path(bib_path).joinpath(bib_file)

        pdf_tasks = build_pdf_task(name, output_path, pandoc_scholar_path,
            csl_file, latex_template, bib_file)
        

        # PANDOC DOCX task
        reference = self.config.get('docx_reference', None)

        docx_tasks = build_docx_task(name, output_path, pandoc_scholar_path,
            csl_file, reference, bib_file)
        
        # FIGURE PNG task
        figpath = self.path.joinpath('figures')
        dpi = self.config.get('image_dpi', 300)
        bg = self.config.get('image_background', 'rgb(255,255,255)')
        opacity = self.config.get('image_opacity', 1.0)

        image_tasks = build_image_task(figpath, dpi, bg, opacity)
        
        tasks = preprocess_tasks + json_tasks + pdf_tasks + docx_tasks + image_tasks
        
        for task in tasks:
            yield task

    def build_analysis_tasks(self):
        
        for _, analysis in self.analysis.items():
            tasks = analysis.build_tasks()
            for task in tasks:
                yield task

    def preprocess(self, force=False):
        return execute_tasks(self.path, {'task_paper': self.build_tasks}, 
            cmd='run', subprocess=True, options=['paper:preprocess'], force=force)
    
    def clean_preprocess(self):
        
        return execute_tasks(self.path, {'task_paper': self.build_tasks}, 
            cmd='clean', subprocess=True, options=['paper:preprocess'])

    def build(self, target=None, force=False):

        if target is None:
            target = self.config.get('build_target', None)
        
        if isinstance(target, str):
            target = ['paper:' + target,]
        elif target is None:
            target = ['paper',]
        else:
            target = ['paper:'+x for x in target]
        
        return execute_tasks(self.path, {'task_paper': self.build_tasks}, 
            cmd='run', subprocess=True, options=target, force=force)
        
    def clean(self, target=None):

        # will clean all paper and image output
        
        #if target is None:
        #    target = self.config.get('build_target', None)

        #if isinstance(target, str):
        #    target = ['paper:' + target,]
        #elif target is None:
        #    target = ['paper',]
        #else:
        #    target = ['paper:'+x for x in target]

        return execute_tasks(self.path, {'task_paper': self.build_tasks}, 
            cmd='clean', subprocess=True, options=['paper', 'image'])
    
    def build_images(self, target=None, force=False):
        
        if isinstance(target, str):
            target = ['image:' + target,]
        elif target is None:
            target = ['image',]
        else:
            target = ['image:'+x for x in target]
        
        if len(list(self.path.joinpath('figures').glob('*.svg')))>0:
            return execute_tasks(self.path, {'task_paper': self.build_tasks}, 
                cmd='run', subprocess=True, options=target, force=force)
        else:
            return None
    
    def clean_images(self, target=None):
        if isinstance(target, str):
            target = ['image:' + target,]
        elif target is None:
            target = ['image',]
        else:
            target = ['image:'+x for x in target]

        return execute_tasks(self.path, {'task_paper': self.build_tasks}, 
            cmd='clean', subprocess=True, options=target)
    
    def create_analysis(self, name=None, clone=None):
        analysis_path = self.path.joinpath('analysis', name)

        if analysis_path.exists():
            raise ValueError('Analysis {} already exists.'.format(name))
        
        # clone is one of:
        # name -> existing analysis in current version
        # version:name -> existing analysis in other version of current project
        # /path/to/project/version:name -> existing analysis in different paper

        if not clone is None:
            if ':' in clone:
                clone_version, clone_analysis = clone.split(':')
            else:
                clone_version, clone_analysis = self.path, clone
            
            try:
                # in case another version in the same project is specified
                clone_version = self.project.versions[clone_version].path
            except:
                pass

            clone_path = pathlib.Path(clone_version).joinpath('analysis', clone_analysis)
            if not clone_path.exists():
                raise ValueError('Cannot clone non-existing analysis {}.'.format(clone))
            shutil.copytree(clone_path, analysis_path)
        else:
            analysis_path.mkdir()
            # create example files
            from .template import analysis_spec, plot_options, code
            with analysis_path.joinpath('spec.yaml').open(mode='w') as f:
                yaml.dump(analysis_spec, stream=f)
            with analysis_path.joinpath('plot_options.yaml').open(mode='w') as f:
                yaml.dump(plot_options, stream=f)
            analysis_path.joinpath('code.py').write_text(code)

    def remove_analysis(self, name=None, clean=True, complete=False):
        
        if not complete:
            if name is None:
                return
            elif not isinstance(name, (list, tuple)):
                name = [name,]

        for k,v in self.analysis.items():
            if complete or k in name:
                if clean:
                    v.clean()
                
                if v.path.exists():
                    shutil.rmtree(v.path)

    def build_analysis(self, name=None, complete=False, force=False, target=None):
        
        if not complete:
            if name is None:
                return
            elif not isinstance(name, (list, tuple)):
                name = [name,]

        for k,v in self.analysis.items():
            if complete or k in name:
                v.build(target=target, force=force)
    
    def clean_analysis(self, name=None, complete=False, target=None):
        
        if not complete:
            if name is None:
                return
            elif not isinstance(name, (list, tuple)):
                name = [name,]

        for k,v in self.analysis.items():
            if complete or k in name:
                v.clean(target=target)

class PaperAnalysis():
    def __init__(self, paper, name=None):
        # paper is PaperVersion
        # name is str
        if isinstance(paper, PaperVersion):
            self._paper = paper
        else:
            path = pathlib.Path(paper).expanduser().resolve()
            self._paper = PaperVersion(path)
            if name is None:
                rel_path = path.relative_to(self._paper.path).parts
                if len(rel_path)>1 and rel_path[0]=='analysis':
                    name = rel_path[1]

        if name is None:
            raise ValueError("Name of analysis is missing.")
        
        self._name = name

    @property
    def paper(self):
        return self._paper
    
    @property
    def path(self):
        return self._paper.path.joinpath('analysis', self._name)
    
    @property
    def name(self):
        return self._name

    def build_tasks(self):
        
        spec = self.load_spec()
        plot_options = self.load_plot_options()

        with add_path(self.path.parent):
            
            summary_tasks = build_summary_tasks(
              spec.get('summary', []),
              self.paper.path.joinpath('variables'),
              self.name)
            
            table_tasks = build_table_tasks(
              spec.get('tables', []),
              self.paper.path.joinpath('tables'),
              self.name)

            figure_tasks = build_figure_tasks(
                self.path, 
                spec.get('figures', {}), spec.get('plots', {}),
                plot_options,
                self.paper.path.joinpath('figures'),
                self.name)
            
        tasks = table_tasks + figure_tasks + summary_tasks

        for task in tasks:
            yield task

    # load analysis spec and plot options
    def load_spec(self, filename='spec'):
        # spec.yaml:
        #  summary: functions/args
        #  table: functions/args
        #  figure: layout
        #  plots: functions/args/style
        
        file = self.path.joinpath('{}.yaml'.format(filename))

        spec = {}

        if file.exists():
            with file.open(mode='r') as stream:
                spec = yaml.load(stream)
        
        return spec

    def load_plot_options(self, filename='plot_options', **kwargs):
        # plot_options.yaml: (local, paper version, project, user)
        #  named colors
        #  default plot style
        #  named styles

        def merge_styles(left, right, key):
            if key=='style':
                if not isinstance(left, list):
                    left = [left,]
                if not isinstance(right, list):
                    right = [right,]
                return left + right
            else:
                return right

        options = [
            {},
            UserConfig(filename=filename),
            YamlConfig(self.paper.project.path, filename=filename),
            YamlConfig(self.paper.path, filename=filename),
            YamlConfig(self.path, filename=filename),
            kwargs,
        ]
        
        options = dict_merge(*options, merge=merge_styles)

        # convert colors to hex
        import matplotlib
        options['colors'] = {k:matplotlib.colors.to_hex(v) for k,v in options.get('colors',{}).items()}

        return options

    def build(self, target=None, force=False):
        base = 'analysis-{}'.format(self.name)
        if isinstance(target, str):
            target = [base + ':' + target]
        elif target is None:
            target = [base]
        else:
            target = [base+':'+x for x in target]

        return execute_tasks(self.paper.path, {'task_analysis': self.build_tasks}, 
            cmd='run', subprocess=True, options=target, force=force)

    def clean(self, target=None):
        base = 'analysis-{}'.format(self.name)
        if isinstance(target, str):
            target = [base + ':' + target]
        elif target is None:
            target = [base]
        else:
            target = [base+':'+x for x in target]

        return execute_tasks(self.paper.path, {'task_analysis': self.build_tasks}, 
            cmd='clean', subprocess=True, options=target)

